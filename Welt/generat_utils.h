#pragma once
#include <random>
#include <vector>
#include <iostream>
#include <unordered_set>
#include <array>
#include "random.h"
#include "biomes.h"

struct point {
	int x, y, orientation;

	point() : orientation(-1), x(INT32_MIN), y(INT32_MIN) {}
	point(int x, int y, int orientation = -1) : x(x), y(y), orientation(orientation) {}
};


std::ostream& operator<<(std::ostream& os, const point& p);

struct rect {
	std::array<point, 4> pts;
	int size;
	biome biom;
};

struct continent {
	struct fitting {
		int translateX = 0,
			translateY = 0;
		float zoom_perc = 100;
	};

	fitting fit;
	std::vector<rect> rects;
	int minX, minY, maxX, maxY;

	continent() : minX(INT32_MAX), minY(INT32_MAX), maxX(INT32_MIN), maxY(INT32_MIN) {}

	void add_rect(const rect& r) {
		minX = std::min(minX, r.pts[0].x);
		minY = std::min(minY, r.pts[0].y);
		maxX = std::max(maxX, r.pts[3].x);
		maxY = std::max(maxY, r.pts[3].y);
		rects.push_back(r);
	}
};

void insert_conn_pts_by_orientation(const rect& next_rect, int center_orientation, std::vector<point>& connPts);
const point& get_random_point(const std::vector<point>& connPts, std::unordered_set<size_t>& usedIdxs, random_generator& rnd);
continent generate_continent(int _size, int world_height, int global_warming_level, int minx, int miny, int maxx, int maxy, random_generator& rnd);
std::vector<continent> generate_continents_parallel(int world_height, int global_warming_level, int count, int minx, int miny, int maxx, int maxy, random_generator& rnd);
std::vector<continent> generate_continents_singlethread(int world_height, int global_warming_level, int count, int minx, int miny, int maxx, int maxy, random_generator& rnd);
rect random_rect(int rect_size, int minx, int miny, int maxx, int maxy, random_generator& rnd);
rect rect_by_center(int _rect_size, const point& center);