#pragma once
#include <stdint.h>
#include "random.h"

enum struct biome : uint8_t {
	OCEAN = 0, DESERT, JUNGLE, STEPPE, FOREST, TAIGA, TUNDRA, POLAR, ROCKS, LAVA
};


biome get_biome_for_ypos(int y_pos, int world_height, int global_warming_level, int rect_size, random_generator& rnd);


struct color { uint8_t r = 0, g = 0, b = 0; };
constexpr static color biome_to_color(biome b) {
	switch (b)
	{
	case biome::OCEAN:
		return color{ 0, 15, 140 };
	case biome::DESERT:
		return color{ 255, 191, 0 };
	case biome::JUNGLE:
		return color{ 101, 154, 101 };
	case biome::STEPPE:
		return color{ 255, 230, 151 };
	case biome::FOREST:
		return color{ 30, 176, 30 };
	case biome::TAIGA:
		return color{ 62, 240, 124 };
	case biome::TUNDRA:
		return color{ 222, 183, 255 };
	case biome::POLAR:
		return color{ 218, 248, 254 };
	case biome::ROCKS:
		return color{ 100, 100, 100 };
	case biome::LAVA:
		return color{ 255, 12, 0 };
	default:
		return color{ 0, 0, 0 };
	}
}