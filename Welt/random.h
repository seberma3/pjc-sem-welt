#pragma once 
#include <functional>
#include <random>
//
//void set_random_seed(int _seed);
//int random(int min, int max);

using random_generator = std::mt19937;

int random(int min, int max, std::mt19937& generator);

random_generator get_random_generator(int seed);