#include "generat_utils.h"
#include <thread>

using namespace std;


continent generate_continent(int _size, int world_height, int global_warming_level, int minx, int miny, int maxx, int maxy, random_generator& rnd) {
	continent c;
	//int _size = random(6, 15);
	rect firstRec = random_rect(_size, minx, miny, maxx, maxy, rnd);
	c.add_rect(firstRec);
	//firstRec.biom = get_biome_for_rect(firstRec, world_height);
	unordered_set<size_t> usedIdxs;
	vector<point> connPts, smoothPts;
	connPts.insert(connPts.end(), firstRec.pts.begin(), firstRec.pts.end());
	smoothPts.insert(smoothPts.end(), firstRec.pts.begin(), firstRec.pts.end());

	float size = _size;
	//int TEST_round = 0;
	while (size > 1.5 /*&& ++TEST_round < 3*/) {
		int i_size = static_cast<int>(size);
		const point& newRectCenter = get_random_point(connPts, usedIdxs, rnd);
		//cout << "Vybran bod " << newRectCenter << " s orientaci " << newRectCenter.orientation << ". Velikost: " << i_size << endl;
		rect nextRect = rect_by_center(i_size, newRectCenter);
		//nextRect.biom = get_biome_for_rect(nextRect, world_height);
		insert_conn_pts_by_orientation(nextRect, newRectCenter.orientation, connPts);
		smoothPts.insert(smoothPts.end(), firstRec.pts.begin(), firstRec.pts.end());
		c.add_rect(nextRect);
		size -= 0.004;
	}

	for (auto& smoothPt : smoothPts) {
		rect smoothRect = rect_by_center(2, smoothPt);
		c.add_rect(smoothRect);
	}

	return c;
}

constexpr int NUM_THREADS = 4;
vector<continent> generate_continents_parallel(int world_height, int global_warming_level, int count, int minx, int miny, int maxx, int maxy, random_generator& rnd) {
	vector<int> continents_sizes;
	for (int i = 0; i < count; i++) {
		continents_sizes.push_back(random(7, 15, rnd));
		for (int i = 0; i < 4; i++)
			continents_sizes.push_back(random(2, 4, rnd));
	}
	
	array<int, 4> random_seeds_for_threads;
	for (int i = 0; i < random_seeds_for_threads.size(); i++)
		random_seeds_for_threads[i] = random(1, 99999, rnd);

	vector<continent> continents; continents.resize(continents_sizes.size());
	list<thread> threads;
	for (int t = 0; t < NUM_THREADS; t++) {
		threads.push_back(thread([&](int tid) {
			random_generator rnd_threadlocal(random_seeds_for_threads[tid]);
			const int chnksize = continents_sizes.size() / NUM_THREADS;
			int beg = tid * chnksize;
			int end = (tid + 1 == NUM_THREADS) ? continents_sizes.size() : beg + chnksize - 1;
			for (int c = beg; c < end; c++) {
				continents[c] = generate_continent(continents_sizes[c], world_height, global_warming_level, minx, miny, maxx, maxy, rnd_threadlocal);
			}
			}, t));
	}
	
	for (auto& th : threads)
		th.join();

	return continents;
}

vector<continent> generate_continents_singlethread(int world_height, int global_warming_level, int count, int minx, int miny, int maxx, int maxy, random_generator& rnd) {
	vector<int> continents_sizes;
	for (int i = 0; i < count; i++)
		continents_sizes.push_back(random(7, 17, rnd));

	for (int i = 0; i < count * 4; i++)
		continents_sizes.push_back(random(2, 4, rnd));
	
	vector<continent> continents;
	for (int i = 0; i < continents_sizes.size(); i++) {
		continents.push_back(generate_continent(continents_sizes[i], world_height, global_warming_level, minx, miny, maxx, maxy, rnd));
	}
	return continents;
}

std::ostream& operator<<(std::ostream& os, const point& p) {
	os << "[" << p.x << "," << p.y << "]";
	return os;
}

const point& get_random_point(const vector<point>& connPts, unordered_set<size_t>& usedIdxs, random_generator& rnd) {
	int rnd_index;
	while (true) {
		rnd_index = random(0, connPts.size(), rnd);
		if (usedIdxs.find(rnd_index) == usedIdxs.end()) {
			usedIdxs.emplace(rnd_index);
			return connPts.at(rnd_index);
		}
	}
}


static void insert_conn_pts_by_orientation(const rect& next_rect, int center_orientation, vector<point>& connPts) {
	switch (center_orientation)
	{
	case 0:
		connPts.push_back(next_rect.pts[0]); connPts.push_back(next_rect.pts[1]); connPts.push_back(next_rect.pts[2]);
		break;
	case 1:
		connPts.push_back(next_rect.pts[0]); connPts.push_back(next_rect.pts[1]); connPts.push_back(next_rect.pts[3]);
		break;
	case 2:
		connPts.push_back(next_rect.pts[0]); connPts.push_back(next_rect.pts[2]); connPts.push_back(next_rect.pts[3]);
		break;
	case 3:
		connPts.push_back(next_rect.pts[1]); connPts.push_back(next_rect.pts[2]); connPts.push_back(next_rect.pts[3]);
		break;
	default:
		throw invalid_argument("center_orientation is not 0,1,2,3");
	}
}


rect rect_by_center(int _rect_size, const point& center) {
	int halbSize = _rect_size / 2;
	rect r;
	point leftTop;
	leftTop.orientation = 0;
	leftTop.x = center.x - halbSize;
	leftTop.y = center.y - halbSize;

	r.pts[0] = leftTop;
	r.pts[1] = point(leftTop.x + _rect_size, leftTop.y, 1);
	r.pts[2] = point(leftTop.x, leftTop.y + _rect_size, 2);
	r.pts[3] = point(leftTop.x + _rect_size, leftTop.y + _rect_size, 3);
	r.size = _rect_size;
	return r;

}

rect random_rect(int rect_size, int minx, int miny, int maxx, int maxy, random_generator& rnd) {
	rect r;
	point leftTop;
	leftTop.orientation = 0;
	leftTop.x = random(minx, maxx, rnd);
	leftTop.y = random(miny, maxy, rnd);

	r.pts[0] = leftTop;
	r.pts[1] = point(leftTop.x + rect_size, leftTop.y, 1);
	r.pts[2] = point(leftTop.x, leftTop.y + rect_size, 2);
	r.pts[3] = point(leftTop.x + rect_size, leftTop.y + rect_size, 3);
	r.size = rect_size;
	return r;
}
