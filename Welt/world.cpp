#include "world.h"
#include <fstream>
#include <array>
#include <list>
#include <queue>
#include <unordered_set>
#include <stdexcept>
#include <memory>
#include <thread>
#include <iostream>
#include <map>
#include "generat_utils.h"
using namespace std;

static void fit_continent(continent& _continent, const world& _world);


world generate_world(const world_params& params, bool multithread, random_generator& rnd) {
	world world(params);
	vector<continent> continents = multithread
		? generate_continents_parallel(world.height, params.global_warming_level, params.continents, 0, 0, world.width, world.height, rnd)
		: generate_continents_singlethread(world.height, params.global_warming_level, params.continents, 0, 0, world.width, world.height, rnd);

	for (auto& _continent : continents) {
		fit_continent(_continent, world);
		for (rect& rct : _continent.rects) {
			rct.biom = get_biome_for_ypos((rct.pts[0].y + rct.pts[3].y) / 2 + _continent.fit.translateY, world.height, world.params.global_warming_level, rct.size, rnd);
			for (int x = rct.pts[0].x; x < rct.pts[3].x; x++)
				for (int y = rct.pts[0].y; y < rct.pts[3].y; y++) {
					int _x = x + _continent.fit.translateX;
					int _y = y + _continent.fit.translateY;
					if (_x < world.width && _y < world.height)
						world.regions[_x][_y] = rct.biom;
				}
		}
	}

	return world;
}


world::world(const world_params& params) : params(params), width(DEFAULT_WIDTH*params.size_coef), height(DEFAULT_HEIGHT*params.size_coef) {
	for (int c = 0; c < width; c++) {
		regions.push_back(vector<biome>());
		for (int r = 0; r < height; r++)
			regions[c].push_back(biome::OCEAN);
	}
}

constexpr int pixel_size = 5;
void world::save_as_img_file(const std::string file_name) {
	ofstream bmpFile(file_name, ios::binary);
	bmpFile << "P6\n" << width * pixel_size << ' ' << height * pixel_size << endl << 255 << endl;
	for (int h = 0; h < height; h++) {
		for (int pxh = 0; pxh < pixel_size; pxh++)
			for (int w = 0; w < width; w++) {
				color c = biome_to_color(regions[w][h]);
				for (int pxh = 0; pxh < pixel_size; pxh++)
					bmpFile.write((char*)& c, sizeof(color));
			}
	}
}

constexpr int STD_MARGIN = 5;
static void fit_continent(continent& cont, const world& w) {
	//Pokud je minusova nejaka souradnice, kontinent se posune do mapy
	if (cont.minX < 0)
		cont.fit.translateX = -cont.minX + STD_MARGIN;
	if (cont.minY < 0)
		cont.fit.translateY = -cont.minY + STD_MARGIN;

	cont.minX += cont.fit.translateX;
	cont.minY += cont.fit.translateY;
	cont.maxX += cont.fit.translateX;
	cont.maxY += cont.fit.translateY;

	//Pokud je presah (nejaka souradnice je prilis velka), posune se kontinent do mapy, pokud se s nim jeste
	//v danem smeru nesoupalo. Jinak se pouzije zoom

	const int presahX = cont.maxX - w.width + 1;
	const int presahY = cont.maxY - w.height + 1;

	if (presahX > 0) {
		if (cont.fit.translateX == 0) { // Jeste se v tomto smeru nehybalo
			int delta_translate = -min(presahX + STD_MARGIN, cont.minX);
			cont.fit.translateX += delta_translate;
			cont.minX += delta_translate;
			cont.maxX += delta_translate;
		}
	}
	if (presahY > 0) {
		if (cont.fit.translateY == 0) { // Jeste se v tomto smeru nehybalo
			int delta_translate = -min(presahY + STD_MARGIN, cont.minY);
			cont.fit.translateY += delta_translate;
			cont.minY += delta_translate;
			cont.maxY += delta_translate;
		}
	}

}
