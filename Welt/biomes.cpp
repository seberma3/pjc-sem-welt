#pragma once
#include <map>
#include <algorithm>
#include "biomes.h"
#include "random.h"

using namespace std;

static map<biome, int> get_biomes_prob(int cold_level, int& probs_sum, int rect_size);

biome get_biome_for_ypos(int ypos, int world_height, int global_warming_level, int rect_size, random_generator& rnd) {
	int cold_level = max(0, min(90, (int)abs((float)ypos * 180 / (float)world_height - 90) - global_warming_level));
	int probs_sum;
	map<biome, int> biome_probs = get_biomes_prob(cold_level, probs_sum, rect_size);
	int rand_val = random(1, probs_sum + 1, rnd) + 1;
	for (auto& biome_prob : biome_probs) {
		rand_val -= biome_prob.second;
		if (rand_val <= 0)
			return biome_prob.first;
	}
	return biome::ROCKS;
}

struct biome_distribution_params{
	const float anti_width_constant;
	const int optimal_cold_level;
	const int weight_in_optimal_cold_level;
}
D_DESERT{ 4,	0,		65 },
D_JUNGLE{ 3,	5,		50 },
D_FOREST{ 1.4,	52,		47 },
D_STEPPE{ 3.2,	30,		37 },
D_TAIGA { 4,	69,		50 },
D_TUNDRA{ 5,	75,		50 },
D_POLAR { 8,	90,		81};



static map<biome, int> get_biomes_prob(int cold_level, int& probs_sum, int rect_size) {
	map<biome, int> probs;
	probs[biome::DESERT] = 5*(int)max(0.0f, -D_DESERT.anti_width_constant * abs(cold_level - D_DESERT.optimal_cold_level) + D_DESERT.weight_in_optimal_cold_level);
	probs[biome::JUNGLE] = 5*(int)max(0.0f, -D_JUNGLE.anti_width_constant * abs(cold_level - D_JUNGLE.optimal_cold_level) + D_JUNGLE.weight_in_optimal_cold_level);
	probs[biome::FOREST] = 5 * (int)max(0.0f, -D_FOREST.anti_width_constant * abs(cold_level - D_FOREST.optimal_cold_level) + D_FOREST.weight_in_optimal_cold_level);
	probs[biome::STEPPE] = 5 * (int)max(0.0f, -D_STEPPE.anti_width_constant * abs(cold_level - D_STEPPE.optimal_cold_level) + D_STEPPE.weight_in_optimal_cold_level);
	probs[biome::TAIGA] = 5 * (int)max(0.0f, -D_TAIGA.anti_width_constant * abs(cold_level - D_TAIGA.optimal_cold_level) + D_TAIGA.weight_in_optimal_cold_level);
	probs[biome::TUNDRA] = 5 * (int)max(0.0f, -D_TUNDRA.anti_width_constant * abs(cold_level - D_TUNDRA.optimal_cold_level) + D_TUNDRA.weight_in_optimal_cold_level);
	probs[biome::POLAR] = 5 * (int)max(0.0f, -D_POLAR.anti_width_constant * abs(cold_level - D_POLAR.optimal_cold_level) + D_POLAR.weight_in_optimal_cold_level);
	if (rect_size < 4)
		probs[biome::ROCKS] = 4;
	if (rect_size < 3)
		probs[biome::LAVA] = 2;

	probs_sum = 0;
	for (auto& pair : probs)
		probs_sum += pair.second;
	return probs;
}