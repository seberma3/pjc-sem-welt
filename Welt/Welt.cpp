﻿#include "Welt.h"
#include "world.h"
#include <string>
#include <algorithm>
#include "random.h"
#include <ctime>
#include <chrono>
using namespace std;

static void run_world_generator(bool multithread);
static void display_help();
static void display_error();

int main(int argc, const char** const args)
{
	if (argc == 1+1 && string(args[1]) == "--help")
		display_help();
	else if (argc == 1+1 && string(args[1]) == "-mt")
		run_world_generator(true);
	else if (argc == 1+0)
		run_world_generator(false); //TODO: FALSE
	else
		display_error();
	return 0;
}

// Proboha, chrono je ale hnusny :-(
template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
	return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}


static void run_world_generator(bool multithread) {
	int seed, num_continents, global_warming_level;
	float world_size_coef;

	cout << "Zadejte random seed: ";
	cin >> seed;
	cout << "Zadejte pocet kontinentu (1 az 256): ";
	cin >> num_continents;
	cout << "Zadejte level globalniho oteplovani (-90 az 90): ";
	cin >> global_warming_level;
	cout << "Zadejte Velikost sveta (0.2 - 10.99): ";
	cin >> world_size_coef;

	random_generator rnd = get_random_generator(seed);
	num_continents = min(max(1, num_continents), 256);
	global_warming_level = min(max(-90, global_warming_level), 90);
	world_size_coef = min(max(0.2f, world_size_coef), 10.99f);

	auto params = world_params(num_continents, global_warming_level, world_size_coef);//-4
	auto beg_time = std::chrono::high_resolution_clock::now();

	multithread = multithread && num_continents >= 8;
	cout << "Svet se bude generovat " << (multithread ? " vice vlakny" : "jednim vlaknem") << endl;
	world w = generate_world(params, multithread, rnd);
	auto end_time = std::chrono::high_resolution_clock::now();
	auto file_name = to_string(time(0));
	cout << "Svet se vygeneroval za " << to_ms(end_time - beg_time).count() << " milisekund." << endl
		<< "Ukladam do souboru " << file_name << ".ppm" << endl;
	w.save_as_img_file(file_name + ".ppm");
	cout << "Svet ulozen. Muzete se pokochat." << endl;
}

static void display_help() {
	cout << "Program generuje nahodne mapy" << endl
		<< "Lze spoustet s parametrem -mt, pak se generuje vicevlaknove." << endl
		<< "Dale program ocekava zadani poctu kontinentu a nazev ciloveho ppm souboru" << endl;
}

static void display_error() {
	cerr << "Chybne parametry" << endl;
}