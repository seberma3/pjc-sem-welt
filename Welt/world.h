#pragma once
#include <string>
#include <vector>
#include "biomes.h"
#include "random.h"

struct world_params
{
private:
	world_params() = delete;
public:
	const int continents;
	const int global_warming_level;
	const float size_coef;

	world_params(int continents, int global_warming_level = 0, float size_coef = 1.0f)
		: continents(continents), global_warming_level(global_warming_level), size_coef(size_coef)
	{}
};

class world;
world generate_world(const world_params& params, bool multithread, random_generator& rnd);

class world {
private:
	static constexpr int DEFAULT_WIDTH = 640, DEFAULT_HEIGHT = 480;
	std::vector<std::vector<biome>> regions;
	const world_params params;
	world(const world_params& params);
public:
	const int width, height;
	void save_as_img_file(const std::string file_name);
	friend world generate_world(const world_params& params, bool multithread, random_generator& rnd);
};

