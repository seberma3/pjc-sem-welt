#include "random.h"
#include <memory>
//
//static int seed = 19;
//static std::unique_ptr<std::mt19937> random_generator;
//
//void set_random_seed(int _seed) {
//	seed = _seed;
//	random_generator = std::make_unique<std::mt19937>(_seed);
//}
//
//int random(int min, int max) {
//	/*int rnd = static_cast<int>((*random_generator)());
//	return std::abs(min + (rnd % (max - min)));*/
//	return random(min, max, *random_generator);
//}

random_generator get_random_generator(int seed){
	return random_generator(seed);
}

int random(int min, int max, random_generator& generator) {
	int rnd = static_cast<int>(generator());
	return std::abs(min + (rnd % (max - min)));
}